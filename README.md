# threejs-hackmitin-2020

Repositorio para el taller de gráficos 3d animados en three js

## ¿Qué es Three.js?

Three.js es un framework (interfaz que nos ayuda a trabajar problemas complejos de manera más intuitiva) que nos permite trabajar gráficos webGL enfocado a objetos 3D, animaciones y renderizaciones.

Para mayores referencias, pueden revisar los [ejemplos](https://threejs.org/examples/) o la [página oficial](https://threejs.org/)

[repo oficial](https://github.com/mrdoob/three.js/)

## Básicos javascript y requerimientos

Three.js está implementado en javascript y para trabajar con él, es necesario tener algunas nociones básicas del lenguaje.

- variables:

```javascript
let variable = 6;
let color = 'rojo';
let objeto = {};
```

- funciones:

```javascript

function cambiarAlgo( argumento1, argumento2 ){
  // hacer algo
  return "valor a regresar"
}
```
- Objetos:

```javascript

let objeto = {
  propiedad1: "valor1",
  propiedad2: "valor2"
};

objeto.propiedad1; // valor1
objeto.propiedad2; // valor2
```


Además al estar embebido en html, se recomienda probarlo en un servidor local:

usando python:

```bash 
python3 -m http.server
```

usando nodejs

```bash 
npm install http-server -g 

http-server
```

## Instalación Three.js

para instalar three js necesitamos un archivo html, un canvas para renderizar y tener una copia de three js [https://threejs.org/build/three.js](https://threejs.org/build/three.js)

es posible generar un proyecto para nodejs e instalar el paquete three con:

```
npm install three --save
```

THREE js está mudando todo hacia la notación de módulos de Emacscript 6, por lo que es necesario tomar en cuenta esto.

## Scene, camera, render

Para poder visualizar el entorno de threejs, necesitamos 3 cosas, una escena, una cámara y un render.

```javascript
const scene = new THREE.Scene()
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
const renderer = const renderer = new THREE.WebGLRenderer();

renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
```

podemos probar con un cubito:

```javascript
const geometry = new THREE.BoxGeometry();
const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
const cube = new THREE.Mesh( geometry, material );
scene.add( cube );

camera.position.z = 5;
```

y lo animamos:

```javascript
function animate() {
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
}
animate();
```

## Mesh

La parte básica de three js son los meshes, básicamente formas que se componen de dos partes: un esqueleto o geometry y un material que le da color o textura a una geometría.

### Geometrías

existen múltiples geometrías disponibles en three js

- cubos

[https://threejs.org/docs/#api/en/geometries/BoxGeometry](https://threejs.org/docs/#api/en/geometries/BoxGeometry)

```
BoxGeometry(width : Float, height : Float, depth : Float, widthSegments : Integer, heightSegments : Integer, depthSegments : Integer)
```

```javascript
const geometry = new THREE.BoxGeometry( 1, 1, 1 );
const material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
const cube = new THREE.Mesh( geometry, material );
scene.add( cube );
```

- cilindros

[https://threejs.org/docs/#api/en/geometries/CylinderGeometry](https://threejs.org/docs/#api/en/geometries/CylinderGeometry)

```
CylinderGeometry(radiusTop : Float, radiusBottom : Float, height : Float, radialSegments : Integer, heightSegments : Integer, openEnded : Boolean, thetaStart : Float, thetaLength : Float)
```


- planos

[https://threejs.org/docs/#api/en/geometries/PlaneGeometry](https://threejs.org/docs/#api/en/geometries/PlaneGeometry)

```
PlaneGeometry(width : Float, height : Float, widthSegments : Integer, heightSegments : Integer)
```

```javascript
const geometry = new THREE.PlaneGeometry( 5, 20, 32 );
const material = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
const plane = new THREE.Mesh( geometry, material );
scene.add( plane );
```


- esferas

[https://threejs.org/docs/#api/en/geometries/SphereGeometry](https://threejs.org/docs/#api/en/geometries/SphereGeometry)

```
SphereGeometry(radius : Float, widthSegments : Integer, heightSegments : Integer, phiStart : Float, phiLength : Float, thetaStart : Float, thetaLength : Float)
```

```javascript
const geometry = new THREE.SphereGeometry( 5, 32, 32 );
const material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
const sphere = new THREE.Mesh( geometry, material );
scene.add( sphere );
```

- toroides

[https://threejs.org/docs/#api/en/geometries/TorusGeometry](https://threejs.org/docs/#api/en/geometries/TorusGeometry)

```
TorusGeometry(radius : Float, tube : Float, radialSegments : Integer, tubularSegments : Integer, arc : Float)
```

```javascript
const geometry = new THREE.TorusGeometry( 10, 3, 16, 100 );
const material = new THREE.MeshBasicMaterial( { color: 0xffff00 } );
const torus = new THREE.Mesh( geometry, material );
scene.add( torus );
```

- tetrahedros

[https://threejs.org/docs/#api/en/geometries/TetrahedronGeometry](https://threejs.org/docs/#api/en/geometries/TetrahedronGeometry)

```
TetrahedronGeometry(radius : Float, detail : Integer)

```

- tubos

[https://threejs.org/docs/#api/en/geometries/TubeGeometry](https://threejs.org/docs/#api/en/geometries/TubeGeometry)

### Materiales

Son los que permiten añadirle cualidades visuales  a los meshes, es importante considerar que algunos materiales pueden llegar a ser más pesados que otros.

- MeshBasicMaterial

Es el material más básico, no genera sombras ni profundidad

[https://threejs.org/docs/#api/en/materials/MeshBasicMaterial](https://threejs.org/docs/#api/en/materials/MeshBasicMaterial)

- MeshNormalMaterial

Cambia los colores dependiendo de las caras a donde esté apuntando.

[https://threejs.org/docs/#api/en/materials/MeshNormalMaterial](https://threejs.org/docs/#api/en/materials/MeshNormalMaterial)

- MeshLambertMaterial

Material que utiliza materiales de reflectancia lambertiana, simula materiales no brillantes, como madera.

[https://threejs.org/docs/#api/en/materials/MeshLambertMaterial](https://threejs.org/docs/#api/en/materials/MeshLambertMaterial)

- MeshPhongMaterial

similar a MeshLambertMaterial, responde a las luces pero añade un superficie metálica.

[https://threejs.org/docs/#api/en/materials/MeshPhongMaterial](https://threejs.org/docs/#api/en/materials/MeshPhongMaterial)

- MeshStandardMaterial

Añade cualidades de rugosidad y metal

[https://threejs.org/docs/#api/en/materials/MeshStandardMaterial](https://threejs.org/docs/#api/en/materials/MeshStandardMaterial)


- Otros materiales:

[http://blog.cjgammon.com/threejs-materials](http://blog.cjgammon.com/threejs-materials)
[https://threejsfundamentals.org/threejs/lessons/threejs-shadows.html](https://threejsfundamentals.org/threejs/lessons/threejs-shadows.html)


## Luces y sombras

Existen muchos tipos de luces, dependiendo de su comportamiento y de las necesidades.
Hay que considerar que las luces añaden peso al renderer, hay que conserver el número de luces al mínimo y si es posible usar sombras bakeadas.


- AmbientLight

Luz general de todo el ambiente

[https://threejs.org/docs/#api/en/lights/AmbientLight](https://threejs.org/docs/#api/en/lights/AmbientLight)

- HemisphereLight

Toma luz del cielo y de la tierra mezclando entre dos objetos


[https://threejs.org/docs/#api/en/lights/HemisphereLight](https://threejs.org/docs/#api/en/lights/HemisphereLight)

- DirectionalLight

usado usualmente para representar al sol

[https://threejs.org/docs/#api/en/lights/DirectionalLight](https://threejs.org/docs/#api/en/lights/DirectionalLight)

- PointLight

luz en forma de punto que lanza luz a todas las direcciones 

[https://threejs.org/docs/#api/en/lights/PointLight](https://threejs.org/docs/#api/en/lights/PointLight)


- otras luces:

[https://threejsfundamentals.org/threejs/lessons/threejs-lights.html](https://threejsfundamentals.org/threejs/lessons/threejs-lights.html)



Las sombras son generadas por el render de three js a partir de shadow maps, por cada luz, un nuevo mapa es renderizado, así que por ejemplo si tienes 5 luces, el renderizado se va a generar 6 veces.

[https://threejsfundamentals.org/threejs/lessons/threejs-shadows.html](https://threejsfundamentals.org/threejs/lessons/threejs-shadows.html)

## Animando objetos

Es posible darle animaciones a los objetos mesh, modificando las propiedades de la geometría, material y en general el mesh propio, algunas propiedades son:

- position:

```javascript
const geometry = new THREE.BoxGeometry();
const material = new THREE.MeshPhongMaterial({
  color: 0xFF0000,    // red (can also use a CSS color string here)
});
const cube = new THREE.Mesh( geometry, material );

cube.position.x += 0.1
cube.position.y += 0.1
```

- rotation:


```javascript
const geometry = new THREE.BoxGeometry();
const material = new THREE.MeshPhongMaterial({
  color: 0xFF0000,    // red (can also use a CSS color string here)
});
const cube = new THREE.Mesh( geometry, material );

cube.rotation.x += 0.1
```

- scale:

```javascript
const geometry = new THREE.BoxGeometry();
const material = new THREE.MeshPhongMaterial({
  color: 0xFF0000,    // red (can also use a CSS color string here)
});
const cube = new THREE.Mesh( geometry, material );

cube.scale.x = 2
```

es posible además utilizar funciones para animar los objetos programáticamente.

- setInterval

ejecuta una funcion cada x tiempo, para detenerla es necesario ejecutar `clearInterval`

```javascript
let lapso = 100 #100 milisegundos
let id = setInterval(function(){
  cube.rotation.x += 0.1
},lapso )
```

- setTimeout

ejecuta una sola vez una funcion tras pasar x tiempo, para detenerla es necesario ejecutar `clearInterval`

```javascript
let lapso = 3000 #3 segundos
let id = setTimeout(function(){
  cube.rotation.x += 0.1
},lapso )
```

- requestAnimationFrame

Ejecuta en la mayor rapidez posible la funcion, es necesario una función recursiva

```javascript
function animar(){
  cube.rotation.x += 0.1
  requestAnimationFrame(animar)
}

animar()
```

## deshacerse de los objetos

Es algo importante a considerar, para evitar que se queden en memoria

[https://discourse.threejs.org/t/dispose-things-correctly-in-three-js/6534](https://discourse.threejs.org/t/dispose-things-correctly-in-three-js/6534)

```javascript
material.dispose()
geometry.dispose()

scene.remove(mesh)

renderer.renderLists.dispose();

```

## Controls

Son módulos creados por los usuarois, usados para moverse a través del espacio, darle interactividad al usuario o dinámica al espacio.

- OrbitControls

usado para orbitar alrededor de un punto 

[https://threejs.org/docs/#examples/en/controls/OrbitControls](https://threejs.org/docs/#examples/en/controls/OrbitControls)

```
OrbitControls( object : Camera, domElement : HTMLDOMElement )
```

y para implementarlo es necesario hacer un update del control:


```javascript
const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

const scene = new THREE.Scene();

const camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 10000 );

const controls = new OrbitControls( camera, renderer.domElement );

//controls.update() must be called after any manual changes to the camera's transform
camera.position.set( 0, 20, 100 );
controls.update();

function animate() {
	requestAnimationFrame( animate );
	// required if controls.enableDamping or controls.autoRotate are set to true
	controls.update();
	renderer.render( scene, camera );
}
```

- PointerLockControls

Controla el puntero usandolo para rotar la cámara, es posible controlar el movimiento tamién

[https://threejs.org/docs/#examples/en/controls/PointerLockControls](https://threejs.org/docs/#examples/en/controls/PointerLockControls)


```javascript
const controls = new PointerLockControls( camera, document.body );

// add event listener to show/hide a UI (e.g. the game's menu)

controls.addEventListener( 'lock', function () {

	menu.style.display = 'none';

} );

controls.addEventListener( 'unlock', function () {

	menu.style.display = 'block';

} );
```

otros controles:

[https://threejs.org/docs/#examples/en/controls/DragControls](https://threejs.org/docs/#examples/en/controls/DragControls)

[https://threejs.org/docs/#examples/en/controls/FlyControls](https://threejs.org/docs/#examples/en/controls/FlyControls)

[https://threejs.org/docs/#examples/en/controls/DeviceOrientationControls](https://threejs.org/docs/#examples/en/controls/DeviceOrientationControls)

## Loaders

Es posible cargar contenido externo a three js, imágenes, videos glbs


- imágenes

para las texturas usamos TextureLoader

[https://threejs.org/docs/#api/en/loaders/TextureLoader](https://threejs.org/docs/#api/en/loaders/TextureLoader)

```javascript
const texture = new THREE.TextureLoader().load( 'textures/land_ocean_ice_cloud_2048.jpg' );

// immediately use the texture for material creation
const material = new THREE.MeshBasicMaterial( { map: texture } );
```

- videos

Para videos usamos VideoTexture, es necesario tener un elemento video con el video a cargar.

[https://threejs.org/docs/#api/en/textures/VideoTexture](https://threejs.org/docs/#api/en/textures/VideoTexture)

[https://www.w3schools.com/html/html5_video.asp](https://www.w3schools.com/html/html5_video.asp)

```javascript
// assuming you have created a HTML video element with id="video"
const video = document.getElementById( 'video' );
const texture = new THREE.VideoTexture( video );
```

- GLTF  

Es posible cargar modelos 3d creados en programas externos, como blender.

[https://threejs.org/docs/#examples/en/loaders/GLTFLoader](https://threejs.org/docs/#examples/en/loaders/GLTFLoader)

```javascript
// Instantiate a loader
const loader = new GLTFLoader();


// Load a glTF resource
loader.load(
	// resource URL
	'models/gltf/duck/duck.gltf',
	// called when the resource is loaded
	function ( gltf ) {

		scene.add( gltf.scene );

		gltf.animations; // Array<THREE.AnimationClip>
		gltf.scene; // THREE.Group
		gltf.scenes; // Array<THREE.Group>
		gltf.cameras; // Array<THREE.Camera>
		gltf.asset; // Object

	},
	// called while loading is progressing
	function ( xhr ) {

		console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

	},
	// called when loading has errors
	function ( error ) {

		console.log( 'An error happened' );

	}
);

```

## Recomendaciones de three js

- intentar usar en lo posible la notación es6.
- Mantener separado en archivos las partes importantes.

```
.
├── glb
├── img
├── main.js
├── parts
│   ├── avatar.js
│   ├── edificios.js
│   └── piso.js
└── video

```
- intentar reutilizar materiales y geometrias lo más que se pueda.
- destruir apropiadamente los objetos, para evitar estar en memoria
- usar la menor cantidad de luces
- mantener las texturas al tamaño mínimo para evitar consumir mucha memoria.
- Tener un on resize para modificar la resolucion del render cuando el usuario cambie el tamaño de la pantalla.


[https://discoverthreejs.com/tips-and-tricks/](https://discoverthreejs.com/tips-and-tricks/)
